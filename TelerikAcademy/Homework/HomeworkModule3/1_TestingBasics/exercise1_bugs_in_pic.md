**Bugs in picture:**

1.  Sign in text and Sign up button are not aligned

2.  Commands are not consistent – use Sign in or Login, instead of using both.

3.  The text in Sign up and Login buttons is not centralized

4.  Twitter logo to be fixed

5.  Facebook needs to be written with capital f

6.  Login via Twitter and Login via facebook buttons – elements are not aligned
    the space between the logo and the text is larger in Twitter button than in
    Facebook button

7.  Unclear credentials needed:

    1.  For username – Email or username instead of “Email or login”

    2.  For password – the name of the field is missing.

8.  “Forgot password” function is missing
