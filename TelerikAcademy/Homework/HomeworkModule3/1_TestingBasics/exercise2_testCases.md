ID: Comment-1

Title: Comment if not logged in

Precondition: Open https://schoolforum.telerikacademy.com/

Steps to reproduce:

1.  Choose topic "Welcome to Telerik Academy Forum"

2.  Check for the "Reply" button under every comment

Expected result: Reply button is unavailable

Severity: High

\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*

ID: Comment-2

Title: Like if not logged in

Precondition: Open https://schoolforum.telerikacademy.com/

Steps to reproduce:

1.  Choose topic "Welcome to Telerik Academy Forum"

2.  Push "like" button (heart) under the first comment

Expected result: Log in window is shown - likes are forbbiden if the user is not
logged in

Severity: Medium

\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*

ID: Comment-3

Title: Share if not logged in

Precondition: Open https://schoolforum.telerikacademy.com/

Steps to reproduce:

1.  Choose topic "Welcome to Telerik Academy Forum"

2.  Push "share" button (chain) under the first comment

Expected result: Log in window is shown - sharing is forbbiden if the user is
not logged in

Severity: Medium

\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*

ID: Comment-4

Title: Writing in the comment field

Precondition:

1.  Open <https://schoolforum.telerikacademy.com/>

2.  Log in

3.  Choose topic " Selenium test topic - Slaveya"

Steps to reproduce: Click Reply button under the topic description

Expected result: Comment's input window is visualizied

Severity: Blocking

\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*

ID: Comment-5

Title: Posting a comment

Precondition:

1.  Open <https://schoolforum.telerikacademy.com/>

2.  Log in

3.  Choose topic " New test topic Sikuli - Slaveya"

Steps to reproduce:

1.  Click Reply button under the topic description

2.  In the text field write the text “A test comment”

3.  Click Reply button

Expected result:

1.  The input window is closed

2.  The text is visualized as a new comment

3.  The comment has all the options: - like - bookmark - share - delete - reply

Severity: Blocking

\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*

ID: Comment-6

Title: Text pre-formatting - bold and italic

Precondition:

1.  Open <https://schoolforum.telerikacademy.com/>

2.  Log in

3.  Choose topic "Welcome to Telerik Academy Forum"

4.  Click Reply button under the topic description

Steps to reproduce:

1.  Push chosen option's button

2.  Write a random text in the input field

3.  Click Replay button

Expected result:

At step 2 - The text is formatted and shown in the preview window

At step 3: - The input window is closed; - the formatted text is shown as a new
comment

Severity: Medium

\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*

ID: Comment-7

Title: Comment's text post-formatting

Precondition:

1.  Open <https://schoolforum.telerikacademy.com/>

2.  Log in

3.  Choose topic "Welcome to Telerik Academy Forum"

4.  Click Reply button under the topic description

Steps to reproduce:

1.  Write a random text

2.  Select it

3.  Push chosen option's button

4.  Click Replay button

Expected result:

At step 1 - the text is shown in the preview field without any formatting

At step 3 - the text is formatted

At step 4: - The input window is closed, the formatted text is shown as a new
comment

Severity: Medium

\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*

ID: Comment-8

Title: Quote option

Precondition:

1.  Open <https://schoolforum.telerikacademy.com/>

2.  Log in

3.  Choose topic "Welcome to Telerik Academy Forum"

4.  Click Reply button under the topic description

Steps to reproduce:

1.  Push Quote button

2.  Write a random text under the quoted text

3.  Push Reply button

Expected result:

At step 1 - commented text and details are shown in the input field with quoted
opening and closing tag - quoted text is shown in the preview field, containing
autor. The quoted text has darker backgroung.

At step 2 - the comment is visualized without a background under the quoted text
(and autor)

At step 3 - the text is visualized as a new comment

Severity: Low

\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*

ID: Comment-9

Title: Picture in a comment

Precondition:

1.  Open <https://schoolforum.telerikacademy.com/>

2.  Log in

3.  Choose topic "Welcome to Telerik Academy Forum"

4.  Click Reply button under the topic description

Steps to reproduce: Push the image button

Expected result: A picture adding menu is visualized, containing:

\- an option to choose From my device with a button Choose files

\- a text field, containing the chosen file's name and an instruction,

\- option to choose From web

\- drag and drop instruction

\- Upload button

\- Cancel button

Severity: Medium

\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*

ID: Comment-10

Title: Picture in a comment - choose from device

Precondition:

1.  Open <https://schoolforum.telerikacademy.com/>

2.  Log in

3.  Choose topic ""Welcome to Telerik Academy Forum""

4.  Click Reply button under the topic description

5.  Push the image button

Steps to reproduce:

1.  Select From my device option

2.  Push Choose Files button

3.  Select a random file - jpg, jpeg, png or gif

4.  Click Upload button

Ecpected result:

Step 2 - File Explorer is opened

Step 3 - the file is loaded (its name is visualised on the right side of the
button)

Step 4 - the picture is added in the comment

Severity: Medium

\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*

ID: Comment-11

Title: Picture in a comment - choose file from the device which is not a picture
format

Precondition:

1.  Open <https://schoolforum.telerikacademy.com/>

2.  Log in

3.  Choose topic ""Welcome to Telerik Academy Forum""

4.  Click Reply button under the topic description

5.  Push the image button

Steps to reproduce:

1.  Select From my device

2.  Push Choose Files button

3.  Select a random file - except jpg, jpeg, png or gif

4.  Click Upload button

Ecpected result: Error massage is shown

Severity: Medium

\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*

ID: CreateATopic-1

Title: Create a new topic if not logged in

Steps to reproduce:

1.  Open <https://schoolforum.telerikacademy.com/>

Expected result: New Topic button is not shown to users who are not logged in

Severity: High

\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*

ID: CreateATopic-2

Title: Create a new topic as a logged in user

Precondition:

1.  Open <https://schoolforum.telerikacademy.com/>

2.  Log in

Steps to reproduce:

1.  Push + New Topic button

2.  In the title field write a random text

3.  In the topic description field write a random text at least 10 symbols long

4.  Click + Create Topic button

Expected result:

At step 1: a window containing options for creation of a new topic is
visualized:

\- A title input field

\- Categories drop down menu, containing suggested categories to choose from and
a search field

\- optional tags field, visualizing suggested tags to choose from and a search
or crate field

\- Topic’s description input field containing text editing options

\- + Create Topic button

\- Cancel button

At step 4 – the topic is created and visualized in a new page, containing:

\- Topic’s title

\- Title editing option

\- Vote button and counter

\- Topic’s description

\- Options in the description field to share, flag, edit, bookmark, delete and
reply to the topic

\- A timer showing the time passed from the creation of the topic. On mouseover
the time and date of the creation is shown

\- Buttons for bookmark, share, flag, reply and watching functions

\- Suggested topics list with their titles, replies and views counters and time
passed from the last activity

Severity: Blocking

\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*

ID: CreateATopic-3

Title: Create a topic with existing title

Precondition:

1.  Open <https://schoolforum.telerikacademy.com/>

2.  Log in

3.  Push + New Topic button

Steps to reproduce:

1.  In the title field write the text “Selenium test topic - Slaveya”

2.  In the topic description field write the text “This is a test topic.”

3.  Click + Create Topic button

Expected result: Error massage is shown explaining that the title has already
been used and OK button

Severity: High

\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*

ID: CreateATopic-4

Title: Create a topic without a description

Precondition:

1.  Open <https://schoolforum.telerikacademy.com/>

2.  Log in

3.  Push + New Topic button

Steps to reproduce:

1.  In the title field write a random text

2.  Click + Create Topic button

Expected result: In the description input field an error massage is shown
explaining that the post can’t be empty

Severity: Medium
