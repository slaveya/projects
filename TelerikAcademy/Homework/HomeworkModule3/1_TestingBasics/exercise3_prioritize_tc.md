**Prioritize the test cases for using a microwave. Use priority levels from 1 to
3, where 1 means highest priority.**

1 - Power goes off while the microwave is working.

3 - The display shows accurate time.

1 - The microwave starts when the start button is pressed.

1 - The microwave stops when the stop button is pressed.

2 - Selection of program changes the mode of the microwave.

3 - The light inside turns on when the door is opened.
