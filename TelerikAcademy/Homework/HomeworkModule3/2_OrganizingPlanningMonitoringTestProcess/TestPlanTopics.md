**Test plan**

1.  **Introduction**

This test plan describes the strategy for testing the Telerik Academy forum
<https://schoolforum.telerikacademy.com/>

1.  **Features to be tested**

-   Topic creation functionalities

1.  **Features not to be tested**

-   Comments’ functionalities

-   User management’s functionalities

-   Administrator role’s functionalities

1.  **Testing tasks**

-   Creating test cases

-   Generating testing data

-   Test cases automation

-   Test cases execution – manual and automation

-   Reporting issues

-   Creating a test report

-   Creating an issue report

1.  **Types of testing:**

-   Smoke testing

-   Functional testing

-   Usability testing

1.  **Schedule and timelines**

Total testing time – 6 working days

-   Test cases creation, testing data – 1 day

-   Test case automation – 2 days

-   Test Execution (manual and automated), reporting issues – 2 days

-   Test and issue reports creation – 1 day

1.  **Environmental needs:**

-   Windows 10 laptop with installed:

    -   Firefox

    -   Chrome

    -   IntelliJ Idea Ultimate

1.  **Testing tools:**

-   Maven

-   Selenium WebDriver

-   Junit

-   Log4j

1.  **Exit criteria:**

-   100% of Prio1 tests pass

-   80% of Prio2 tests pass

-   Time ran out
