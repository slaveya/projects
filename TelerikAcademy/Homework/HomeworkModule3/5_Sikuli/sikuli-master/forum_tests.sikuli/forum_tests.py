from _lib import *

    
class ForumTests(unittest.TestCase):
    
    def setUp(self):
        pass
    
    def tearDown(self):
        pass    
        
    def test_002_LogIn(self):
        wait(ForumUI.incognito, 5)
        type(ForumUI.search_field, "https://schoolforum.telerikacademy.com" + Key.ENTER)
        wait (ForumUI.forum_page,3)
        click(ForumUI.login)
        wait(ForumUI.login_page,5)
        type(ForumUI.email, "test_01_telerik_forum@mail.bg")
        type(ForumUI.password, "Testaccount01")
        click(ForumUI.sign_in)
        sleep(3)
        assert(exists(ForumUI.forum_page))
        
    def test_010_Create_existing_topic(self):
        wait(ForumUI.new_topic,5)
        click(ForumUI.new_topic)
        wait(ForumUI.title_field)
        type(ForumUI.title_field, "Selenium test topic - Slaveya")
        type(ForumUI.description_field, "This is a test topic.")
        click(ForumUI.create_topic)

        assert(exists(ForumUI.existing_topic))

        click(ForumUI.error_msg_ok)
        click(ForumUI.cancel_topic) 
        click(ForumUI.abandon_topic)

    def test_011_Find_a_topic_and_comment(self):
        click(ForumUI.search_in_forum_button)
        type(ForumUI.search_in_forum_field, "New test topic Sikuli - Slaveya")
        wait(ForumUI.topic_found)
        click(ForumUI.topic_found)
        wait(ForumUI.found_topic_page)
        click(ForumUI.reply_button)
        type(ForumUI.description_field, "A test comment")
        click(ForumUI.reply_message_button)

        assert(exists(ForumUI.sent_message))
            

    def test_100_CloseCalc(self):
        Chrome.Close()

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(ForumTests)
    
   
    outfile = open("Report.html", "w")
    runner = HTMLTestRunner.HTMLTestRunner(stream=outfile, title='Forum Tests Report' )
    runner.run(suite)
    outfile.close()

