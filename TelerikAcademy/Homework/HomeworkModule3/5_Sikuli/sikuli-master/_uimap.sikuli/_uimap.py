########################################################
# UI map for XYZ
########################################################
from sikuli import *
########################################################

class ForumUI:
    chome = "chrome.PNG"
    new_tab = "new_tab.PNG"
    incognito = "incognito.PNG"
    search_field = "search_field.PNG"
    forum_page = "forum_page.PNG"
    login = "Login.PNG"
    login_page = "login_page.PNG"
    email = "email_field.PNG"
    password = "pass_field.PNG"
    sign_in = "sign_in_button.PNG"

    new_topic = "new_topic_button.PNG"
    title_field = "title_field.PNG"
    description_field = "description_field.PNG"
    create_topic = "create_topic_button.PNG"
    existing_topic = "existing_title_message.PNG"
    error_msg_ok = "error_msg_ok_button.PNG"
    cancel_topic = "cancel_topic_button.PNG"
    abandon_topic = "abandon_topic_button.PNG"

    search_in_forum_button = "search_in_forum_button.PNG"
    search_in_forum_field = "search_in_forum_field.PNG"
    topic_found = "topic_found.PNG"
    found_topic_page = "found_topic_page.PNG"
    reply_button = "reply_button.PNG"
    reply_message_button = "reply_msg_button.PNG"
    sent_message = "sent_msg_text.PNG"




