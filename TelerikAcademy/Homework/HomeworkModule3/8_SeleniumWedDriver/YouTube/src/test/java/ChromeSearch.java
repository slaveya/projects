import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ChromeSearch {
    @Test
    public void findingVideo() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Slaveya\\Documents\\Learning\\Telerik Academy\\Training\\Module 3\\chromedriver.exe");
        WebDriver chromeDriver = new ChromeDriver();
        chromeDriver.get("https://www.youtube.com");

        WebElement search = (new WebDriverWait(chromeDriver, 20))
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"search\"]")));
        search.sendKeys("Selenium Tutorial for Beginners" + (Keys.ENTER));

        WebElement video = (new WebDriverWait(chromeDriver, 20))
                .until(ExpectedConditions.elementToBeClickable(By.linkText("Selenium Tutorial For Beginners | What Is Selenium? | Selenium Automation Testing Tutorial | Edureka")));
        video.click();

        WebElement pause = (new WebDriverWait(chromeDriver, 20))
                .until(ExpectedConditions.elementToBeClickable(By.cssSelector("#movie_player > div.ytp-chrome-bottom > div.ytp-chrome-controls > div.ytp-left-controls > button")));
        pause.click();

        WebElement expand = chromeDriver.findElement(By.xpath("//*[@id=\"player-container-inner\"]"));
        expand.click();

        expand.click();

        chromeDriver.close();
    }
}
