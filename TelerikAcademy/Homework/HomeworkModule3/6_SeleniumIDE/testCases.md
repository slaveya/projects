ID: Comment-5

Title: Posting a comment

Precondition:

1.  Open <https://schoolforum.telerikacademy.com/>

2.  Log in

3.  Choose topic " New test topic Sikuli - Slaveya"

Steps to reproduce:

1.  Click Reply button under the topic description

2.  In the text field write the text “A test comment”

3.  Click Reply button

Expected result:

1.  The input window is closed

2.  The text is visualized as a new comment

3.  The comment has all the options: - like - bookmark - share - delete - reply

Severity: Blocking

\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*

ID: CreateATopic-3

Title: Create a topic with existing title

Precondition:

1.  Open <https://schoolforum.telerikacademy.com/>

2.  Log in

3.  Push + New Topic button

Steps to reproduce:

1.  In the title field write the text “Selenium test topic - Slaveya”

2.  In the topic description field write the text “This is a test topic.”

3.  Click + Create Topic button

Expected result: Error massage is shown explaining that the title has already
been used and OK button

Severity: High
