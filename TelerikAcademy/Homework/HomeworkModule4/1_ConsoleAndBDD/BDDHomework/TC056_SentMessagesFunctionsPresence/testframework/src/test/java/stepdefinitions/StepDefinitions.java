package stepdefinitions;

import com.telerikacademy.testframework.UserActions;
import com.telerikacademy.testframework.Utils;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.sql.Driver;

import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;
import static com.telerikacademy.testframework.Utils.getWebDriver;

public class StepDefinitions extends BaseStepDefinitions{
    UserActions actions = new UserActions();
    WebDriver driver = getWebDriver();

    @Given("User is logged in")
    public void signIn(){
        actions.logIn();
        actions.signIn();
    }

    @Given("Click $element element")
    @When("Click $element element")
    @Then("Click $element element")
    public void clickElement(String element){
        actions.waitForElementVisible(element,20);
        actions.clickElement(element);
    }

    @Given("Type $value in $name field")
    @When("Type $value in $name field")
    @Then("Type $value in $name field")
    public void typeInField(String value, String field){
        actions.waitForElementVisible(field,10);
        actions.typeValueInField(value, field);
    }

    @Then("Assert $element is present")
    public void assertPresence( String locator){
        Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
    }
}
