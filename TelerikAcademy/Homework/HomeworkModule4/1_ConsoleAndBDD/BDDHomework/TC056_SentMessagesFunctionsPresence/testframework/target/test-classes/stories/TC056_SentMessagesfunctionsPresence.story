Meta:
@sentMessagesFunctionalities

Narrative:
As a logged user
I want to be able to share, flag, edit, bookmark delete and reply to messages
So that I will be able to easily communicate and interact with others

Scenario: checking the presence ot the functions of sent messages

Given User is logged in
And Click xpath.currentUserIcon element
And Click xpath.currentUserOptionsPanelMessagesIcon element
When Click xpath.sentMessagesButton element
And Click xpath.sentMessageSelect element
Then Assert xpath.sentMessageFunctionsShare is present
And Assert xpath.sentMessageFunctionsEdit is present
And Click xpath.sentMessageFunctionsMore element
And Assert xpath.sentMessageFunctionsBookmark is present
And Assert xpath.sentMessageFunctionsDelete is present
And Assert xpath.sentMessageFunctionsReply is present