| Test Case ID | Title                                          | Description                                                                                                        | Pre-conditions            | Steps to reproduce                                                                                                | Input data                                                                                   | Expected result                                                                                              | Priority |
|--------------|------------------------------------------------|--------------------------------------------------------------------------------------------------------------------|---------------------------|-------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------|----------|
| userPref 1   | Recently used devices – “log out all” function | When logged in on 2 different devices, check if “Log out all button” is logging the user out from the other device | Go to the URL address on: | From the desktop device click on the “Log out all” button                                                         | URL: <https://schoolforum.telerikacademy.com> User 1 credentials: Username: User 1 Password: | User 1 is logged out from the mobile device                                                                  | Prio 1   |
| userPref 2   | Recently used devices – “Not you” function     | When logged in on 2 different devices, check if ”Not you” function is navigating to the password changing function | Go to the URL address on: | From the desktop device click on the settings button of the mobile device and select “Not you” and check details. | URL: <https://schoolforum.telerikacademy.com> User 1 credentials: Username: User 1 Password: | Pop-up containing details about the other device is visualized with buttons “Secure my account” and “cancel” | Prio 2   |
| userPref 3   | Recently used devices – “log out” function     | When logged in on 2 different devices, check if other’s device “Log out” button is logging the user out from it    | Go to the URL address on: | From the desktop device click on the settings button of the mobile device and select “Log out”                    | URL: <https://schoolforum.telerikacademy.com> User 1 credentials: Username: User 1 Password: | User 1 is logged out from the mobile device                                                                  | Prio 1   |

1.  desktop device

2.  mobile device

3.  Log in to User 1’s account

4.  Check if User 1 is logged out from the mobile device

5.  desktop device

6.  mobile device

7.  Log in to User 1’s account

8.  Click “Secure my account button”

9.  The password changing menu is visualized

10. desktop device

11. mobile device

12. Log in to User 1’s account
