Prerequisits for running IntelliJ project:

1. Maria DB server
2. Heidi 
3. Java 1.8
4. InteliJ Ultimate

Instructions for starting the project:

1. Download the project social-network from: https://gitlab.com/sivancheva/qa_project_teamjune
2. In general MariaDB is running on port 3306. This project is set up to run on port 3307. If you want to change the setting
open the social network project ..\src\main\resources\application.properties and change database.url
3 Open Heidi and run SocialNetworkEmptyDatabase.sql script in order to generate empty database for the tests. Please, have in mind, tests
should run on newly created database!
4. Open IntelliJ project and add MariaDb as database, run the project from SocialNetworkApplication file


Prerequisits for running Postman tests:

1. Postman 7.9.0
2. Newman, npm install -g newman
3. Newman reporter, npm install -g newman-reporter-htmlextra
4. Download the project with postman files "SocialNetwork_TeamJune_PostmanTests"
5. Flash player for playing the videos

Instructions for running the project:

After Social Network application is running on localhost:8080 and installing necessery
software go to the project folder "SocialNetwork_TeamJune_PostmanTests", double click on createReportNewman.bat 
Result will be wisible in a newman folder, as .html file