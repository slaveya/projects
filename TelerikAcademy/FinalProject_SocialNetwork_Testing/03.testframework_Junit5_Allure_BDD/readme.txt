Prerequisites:

Allure Installation

1. download allure https://github.com/allure-framework/allure2/releases/tag/2.13.0
2. Unpack the archive to allure-command-line directory.
3. Navigate to the bin directory.
4. Add allure to system PATH variables
5. Check if allure --version returns result about allure version

Maven (3.6.2) installation

1. Java 1.8, Make sure JDK is installed, and JAVA_HOME environment variable is configured.
2. download Maven: https://maven.apache.org/download.cgi
2.1. Additional info for Maven installation https://www.mkyong.com/maven/how-to-install-maven-in-windows/
3. check if Maven is download: mvn --version

Starting Dev Project social-network

Prerequisites:

1. Maria DB server
2. Heidi
3. Java 1.8
4. IntelliJ Ultimate

Instructions for starting the project:

1. Download the project social-network from: https://gitlab.com/sivancheva/qa_project_teamjune
2. In general Heidi is running on port 3306. This project is set up to run on port 3307. If you want to change the setting
open \src\main\resources\application.properties and change database.url
3 Open Heidi and run Database_ReadyForTest.sql script in order to generate database for the tests. Please, have in mind, tests
should run on newly created database!
3. Open IntelliJ project and add MariaDb as database, run the project from SocialNetworkApplication file

Instructions for running the tests:

1. Download the project 06.testframework_TestNG_Allure from: https://gitlab.com/sivancheva/qa_project_teamjune
2. Tests are configured to run on latest version of Mozilla Firefox.
3. In order to run the tests generate new database, run social-network project and double click on runTests.bat 
4. Allure report will be visible on the screen.



 
    