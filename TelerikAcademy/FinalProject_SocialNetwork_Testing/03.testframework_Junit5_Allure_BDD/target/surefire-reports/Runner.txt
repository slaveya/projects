-------------------------------------------------------------------------------
Test set: Runner
-------------------------------------------------------------------------------
Tests run: 47, Failures: 1, Errors: 2, Skipped: 2, Time elapsed: 96.976 sec <<< FAILURE! - in Runner
BeforeStories  Time elapsed: 1.358 sec
Given I am on a home page  Time elapsed: 0.074 sec
When As registered user I am logging in with UserUI_D and A123456  Time elapsed: 1.318 sec
Then I assert post.publicPostUserE is visible in feed  Time elapsed: 0.084 sec
And I assert post.buddiesOnlyPostUserE is visible in feed  Time elapsed: 0.087 sec
And I assert post.buddiesOnlyPostUserF is not visible in feed  Time elapsed: 0.03 sec
And I assert post.privatePostUserF is not visible in feed  Time elapsed: 0.028 sec
And I assert post.privatePostUserE is not visible in feed  Time elapsed: 0.062 sec  <<< FAILURE!
java.lang.AssertionError: expected:<0> but was:<1>
	at stepdefinitions.StepDefinitions.assertPostIsNotInPersonalizedFeed(StepDefinitions.java:86)

And I assert post.publicPostUserF is visible in feed skipped
@AfterScenario  Time elapsed: 0 sec
Given As registered user I am logging in with UserUI_G and A123456  Time elapsed: 0.891 sec
When I remove UI_F from my buddies  Time elapsed: 3.39 sec
Then I assert buddies.userF is not in my buddy list  Time elapsed: 1.717 sec
When As registered user I am logging in with UserUI_F and A123456  Time elapsed: 0.894 sec
Then I assert post.buddiesOnlyPostUserG is not visible in feed  Time elapsed: 0.021 sec
And I assert post.privatePostUserG is not visible in feed  Time elapsed: 0.022 sec
And I assert post.publicPostUserG is visible in feed  Time elapsed: 30.288 sec  <<< ERROR!
org.openqa.selenium.TimeoutException: Expected condition failed: waiting for visibility of element located by By.xpath: //p[contains(text(),'Public post of UserUI_G')] (tried for 30 second(s) with 500 milliseconds interval)
	at stepdefinitions.StepDefinitions.assertPostIsVisibleInPersonalizedFeed(StepDefinitions.java:81)
Caused by: org.openqa.selenium.NoSuchElementException: 
Cannot locate an element using By.xpath: //p[contains(text(),'Public post of UserUI_G')]
For documentation on this error, please visit: http://seleniumhq.org/exceptions/no_such_element.html
Build info: version: '3.14.0', revision: 'aacccce0', time: '2018-08-02T20:19:58.91Z'
System info: host: 'DESKTOP-LJJHQNA', ip: '192.168.56.1', os.name: 'Windows 10', os.arch: 'amd64', os.version: '10.0', java.version: '1.8.0_191'
Driver info: driver.version: unknown
	at stepdefinitions.StepDefinitions.assertPostIsVisibleInPersonalizedFeed(StepDefinitions.java:81)

@AfterScenario?  Time elapsed: 0 sec
Given As registered user I am logging in with UserUI_B and A123456  Time elapsed: 0.904 sec
When I send buddy request to UI_G  Time elapsed: 4.106 sec
And As registered user I am logging in with UserUI_G and A123456  Time elapsed: 0.892 sec
And Buddy request is accepted  Time elapsed: 30.324 sec  <<< ERROR!
org.openqa.selenium.TimeoutException: Expected condition failed: waiting for element to be clickable: By.xpath: //button[@id='accept request'] (tried for 30 second(s) with 500 milliseconds interval)
	at stepdefinitions.StepDefinitions.acceptBuddyRequest(StepDefinitions.java:60)
Caused by: org.openqa.selenium.NoSuchElementException: 
Cannot locate an element using By.xpath: //button[@id='accept request']
For documentation on this error, please visit: http://seleniumhq.org/exceptions/no_such_element.html
Build info: version: '3.14.0', revision: 'aacccce0', time: '2018-08-02T20:19:58.91Z'
System info: host: 'DESKTOP-LJJHQNA', ip: '192.168.56.1', os.name: 'Windows 10', os.arch: 'amd64', os.version: '10.0', java.version: '1.8.0_191'
Driver info: driver.version: unknown
	at stepdefinitions.StepDefinitions.acceptBuddyRequest(StepDefinitions.java:60)

Then I assert buddies.userB is in my buddy list skipped
@AfterScenario??  Time elapsed: 0.002 sec
Given I am on a home page?  Time elapsed: 0.038 sec
When As registered user I am logging in with UserUI_A and A123456  Time elapsed: 0.873 sec
Then I assert presence on user's page  Time elapsed: 0.063 sec
@AfterScenario???  Time elapsed: 0.002 sec
Given I am on a home page??  Time elapsed: 0.041 sec
When I register with randomUser3 and randomPassword  Time elapsed: 1.486 sec
Then I assert presence on login page  Time elapsed: 0.059 sec
And I am on a home page  Time elapsed: 0.052 sec
When As registered user I am logging in with randomUser3 and randomPassword  Time elapsed: 0.857 sec
Then I assert presence on user's page?  Time elapsed: 0.06 sec
@AfterScenario????  Time elapsed: 0.003 sec
Given I am on a home page???  Time elapsed: 0.065 sec
When I log in UserUI_A and A123456  Time elapsed: 0.9 sec
And I update my personal Information  Time elapsed: 5.767 sec
Then I assert existence of First_Name_Update in updatedUser.firstName  Time elapsed: 0.063 sec
Then I assert existence of Last_Name_Updated in updatedUser.lastName  Time elapsed: 0.078 sec
Then I assert existence of OTHER in updatedUser.gender  Time elapsed: 0.052 sec
Then I assert existence of Town_Of_Birth_Updated in updatedUser.bornIn  Time elapsed: 0.094 sec
Then I assert existence of Country_Of_Residence_Updated in updatedUser.livesIn  Time elapsed: 0.078 sec
Then I assert existence of Update_UI_A@abv.bg in updateUser.email  Time elapsed: 0.078 sec
Then I assert existence of PRIVATE in updateUser.picturePrivacy  Time elapsed: 0.078 sec
@AfterScenario?????  Time elapsed: 0.024 sec
AfterStories  Time elapsed: 0.797 sec
