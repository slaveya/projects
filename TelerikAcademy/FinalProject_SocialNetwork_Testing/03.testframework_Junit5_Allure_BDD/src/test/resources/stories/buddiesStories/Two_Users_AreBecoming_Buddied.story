Meta:
@smoke

Narrative:
As a register user
I want to connect with the other users
So that I am going to be added in their buddy lists

Scenario: Two users are becoming buddies
Given As registered user I am logging in with UserUI_B and A123456
When I send buddy request to UI_G
And As registered user I am logging in with UserUI_G and A123456
And Buddy request is accepted
Then I assert buddies.userB is in my buddy list