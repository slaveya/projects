Meta:
@smoke

Narrative:
As a buddy of a user
I want to remove the user from my buddy list
So that the user will not see my buddies only posts in his/her feed anymore

Scenario: Removing buddies
Given As registered user I am logging in with UserUI_G and A123456
When I remove UI_F from my buddies
Then I assert buddies.userF is not in my buddy list

When As registered user I am logging in with UserUI_F and A123456
Then I assert post.buddiesOnlyPostUserG is not visible in feed
And I assert post.privatePostUserG is not visible in feed
And I assert post.publicPostUserG is visible in feed


