Meta:
@smoke

Narrative:
As a registered user
I want to update my personal inforamtion in my account

Scenario: User updates personal information
Given I am on a home page
When I log in UserUI_A and A123456
And I update my personal Information
Then I assert existence of First_Name_Update in updatedUser.firstName
Then I assert existence of Last_Name_Updated in updatedUser.lastName
Then I assert existence of OTHER in updatedUser.gender
Then I assert existence of Town_Of_Birth_Updated in updatedUser.bornIn
Then I assert existence of Country_Of_Residence_Updated in updatedUser.livesIn
Then I assert existence of Update_UI_A@abv.bg in updateUser.email
Then I assert existence of PRIVATE in updateUser.picturePrivacy




