Meta:
@smoke

Narrative:
As a registered user who has buddies with all types of posts and also there are non-buddies with public posts in the Social Network
I want to log in and reach my feed
So that I can see my buddies' public and 'buddies only' posts and public posts of non-buddies

Scenario: Feed content with buddies' and non-buddies' posts
Given I am on a home page
When As registered user I am logging in with UserUI_D and A123456
Then I assert post.publicPostUserE is visible in feed
And I assert post.buddiesOnlyPostUserE is visible in feed
And I assert post.buddiesOnlyPostUserF is not visible in feed
And I assert post.privatePostUserF is not visible in feed
And I assert post.privatePostUserE is not visible in feed
And I assert post.publicPostUserF is visible in feed
