package com.telerikacademy.testframework;

public class Consts {

    //ERROR MESSAGES
    public static final String ERROR_WRONG_USERNAME_PASSWORD = "Wrong username or password.";

    //USER A UPDATE
    public static final String UPDATED_FIRST_NAME = "First_Name_Updated";
    public static final String UPDATED_LAST_NAME = "Last_Name_Updated";
    public static final String UPDATED_TOWN_OF_BIRTH = "Town_Of_Birth_Updated";
    public static final String UPDATED_COUNTRY_OF_BIRTH = "Country_Of_Birth_Updated";
    public static final String UPDATED_TOWN_OF_RESIDENCE = "Town_Of_Residence_Updated";
    public static final String UPDATED_COUNTRY_OF_RESIDENCE = "Country_Of_Residence_Updated";

    //USER A DATA
    public static final String USER_A_UPDATED_EMAIL = "Update_UI_A@abv.bg";

}