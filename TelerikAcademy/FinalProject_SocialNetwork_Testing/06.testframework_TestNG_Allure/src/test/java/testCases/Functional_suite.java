package testCases;

import com.telerikacademy.testframework.Constants;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.*;

public class Functional_suite extends BaseTest {


    @AfterMethod(groups = "smoke")
    public void logOut() {
           actions.logOut();
    }

    @Severity(SeverityLevel.BLOCKER)
    @Test(groups = "personal")
    public void T01_UserEditPersonalDataToPrivate() {
        actions.logIn(Constants.USER_A_USERNAME, Constants.ALL_USERS_VALID_PASSWORD);
        actions.updateUsersInfo();
        //asserts
        actions.assertTextContains(Constants.UPDATED_FIRST_NAME, "updatedUser.firstName");
        actions.assertTextContains(Constants.UPDATED_LAST_NAME, "updatedUser.lastName");
        actions.assertTextContains(Constants.GENDER_OTHER, "updatedUser.gender");
        actions.assertTextContains(Constants.UPDATED_TOWN_OF_BIRTH, "updatedUser.bornIn");
        actions.assertTextContains(Constants.UPDATED_COUNTRY_OF_BIRTH, "updatedUser.bornIn");
        actions.assertTextContains(Constants.UPDATED_TOWN_OF_RESIDENCE, "updatedUser.livesIn");
        actions.assertTextContains(Constants.UPDATED_COUNTRY_OF_RESIDENCE, "updatedUser.livesIn");
        actions.assertTextContains(Constants.USER_A_UPDATED_EMAIL, "updateUser.email");
        actions.assertTextContains(Constants.PRIVACY_PRIVATE, "updateUser.picturePrivacy");
        actions.assertElementPresent("updatedUser.profilePicture");
    }

    @Severity(SeverityLevel.CRITICAL)
    @Test(groups = {"smoke", "buddies"})
    public void T02_TwoUsersAreBecomingBuddies() {
        actions.usersAreBecomingBuddies(Constants.USER_B_USERNAME, Constants.ALL_USERS_VALID_PASSWORD, Constants.USER_C_USERNAME,
                Constants.USER_C_FIRST_NAME, Constants.ALL_USERS_VALID_PASSWORD);
        actions.assertElementPresent("buddies.userBFriend");
    }

    @Severity(SeverityLevel.CRITICAL)
    @Test(groups = {"smoke", "posts"})
    public void T03_UserCreatesAndDeletesPubicPosts() {
        actions.logIn(Constants.USER_D_USERNAME, Constants.ALL_USERS_VALID_PASSWORD);
        actions.goToUsersPage();
        //posting and deleting PUBLIC post
        actions.createPost("post.postPublic", Constants.POST_EXAMPLE);
        actions.assertElementPresent("post.postExample");
        actions.deleteAPost();
        actions.assertElementNotPresent("post.postExample");
    }

    @Severity(SeverityLevel.CRITICAL)
    @Test(groups = "posts")
    public void T04_UserCreatesAndDeletesPrivatePosts() {
        actions.logIn(Constants.USER_D_USERNAME, Constants.ALL_USERS_VALID_PASSWORD);
        actions.goToUsersPage();

        //posting and deleting PRIVATE post
        actions.createPost("post.postPrivate", Constants.POST_EXAMPLE);
        actions.assertElementPresent("post.postExample");
        actions.deleteAPost();
        actions.assertElementNotPresent("post.postExample");
    }

    @Severity(SeverityLevel.NORMAL)
    @Test(groups = "posts")
    public void T05_UserCreatesAndDeletesBuddiesOnlyPosts() {
        actions.logIn(Constants.USER_D_USERNAME, Constants.ALL_USERS_VALID_PASSWORD);
        actions.goToUsersPage();

        //posting and deleting Buddies only post
        actions.createPost("post.postBuddiesOnly", Constants.POST_EXAMPLE);
        actions.assertElementPresent("post.postExample");
        actions.deleteAPost();
        actions.assertElementNotPresent("post.postExample");
    }

    @Severity(SeverityLevel.NORMAL)
    @Test(groups = "userPagePrivacy")
    public void T06_VerifyIfBuddyCanSeeBuddiesOnlyPostsAtUsersPage() {
        actions.logIn(Constants.USER_D_USERNAME, Constants.ALL_USERS_VALID_PASSWORD);
        actions.assertElementPresent("post.buddiesOnlyPostUserE");
    }

    //expected to fail, privacy is not working
    @Severity(SeverityLevel.NORMAL)
    @Test(groups = "userPagePrivacy")
    public void T07_VerifyIfBuddyCannotSeePrivatePostsAtUsersPage() {
        actions.logIn(Constants.USER_D_USERNAME, Constants.ALL_USERS_VALID_PASSWORD);
        actions.assertElementNotPresent("post.privatePostUserE");
    }

    @Severity(SeverityLevel.NORMAL)
    @Test(groups = "userPagePrivacy")
    public void T08_VerifyIfNonBuddyCanSeePublicPostsAtUsersPage() {
        actions.logIn(Constants.USER_C_USERNAME, Constants.ALL_USERS_VALID_PASSWORD);
        actions.searchForUserAndGoToUsersPage(Constants.USER_E_FIRST_NAME);
        actions.assertElementPresent("post.publicPostUserE");
    }

    //expected to fail, privacy is not working
    @Severity(SeverityLevel.NORMAL)
    @Test
    public void T09_VerifyIfNonBuddyCannotSeePrivatePostsAtUsersPage() {
        actions.logIn(Constants.USER_C_USERNAME, Constants.ALL_USERS_VALID_PASSWORD);
        actions.searchForUserAndGoToUsersPage(Constants.USER_E_FIRST_NAME);
        actions.assertElementNotPresent("post.privatePostUserE");
    }

    //expected to fail, privacy is not working
    @Severity(SeverityLevel.NORMAL)
    @Test(groups = "smoke")
    public void T10_VerifyIfNonBuddyCannotSeeBuddiesOnlyPostsAtUsersPage() {
        actions.logIn(Constants.USER_C_USERNAME, Constants.ALL_USERS_VALID_PASSWORD);
        actions.searchForUserAndGoToUsersPage(Constants.USER_E_FIRST_NAME);
        actions.assertElementNotPresent("post.buddiesOnlyPostUserE");
    }

    @Severity(SeverityLevel.NORMAL)
    @Test(groups = "smoke")
    public void T11_NonBuddyCommentsPublicPost() {
        actions.logIn(Constants.USER_C_USERNAME, Constants.ALL_USERS_VALID_PASSWORD);
        actions.searchForUserAndGoToUsersPage(Constants.USER_E_FIRST_NAME);
        actions.createACommentPublicPost(Constants.COMMENT_EXAMPLE);
        actions.assertTextEquals(Constants.COMMENT_EXAMPLE, "comment.commentUnderPublicPost");
    }

    @Severity(SeverityLevel.NORMAL)
    @Test(groups = "smoke")
    public void T12_NonBuddyLikeAndDislikePublicPost() {
        actions.logIn(Constants.USER_C_USERNAME, Constants.ALL_USERS_VALID_PASSWORD);
        actions.searchForUserAndGoToUsersPage(Constants.USER_E_FIRST_NAME);
        actions.likePublicPost();
        actions.likePublicPost();
        actions.assertTextEquals("0", "comment.likesCounterPublicPost");
    }

    @Severity(SeverityLevel.NORMAL)
    @Test(groups = "posts")
    public void T13_VerifyUserCannotCreateEmptyPost() {
        actions.logIn(Constants.USER_D_USERNAME, Constants.ALL_USERS_VALID_PASSWORD);
        actions.goToUsersPage();
        actions.createPost("post.postPublic", "");
        actions.assertElementNotPresent("post.postExample");
    }

    //expected to fail, privacy is not working
    @Severity(SeverityLevel.NORMAL)
    @Test(groups = "feed")
    public void T14_VerifyPersonalizedFeedContent() {
        actions.logIn(Constants.USER_D_USERNAME, Constants.ALL_USERS_VALID_PASSWORD);
        actions.assertElementPresent("post.buddiesOnlyPostUserE");
        actions.assertElementPresent("post.publicPostUserE");
        actions.assertElementPresent("post.publicPostUserF ");
    }

    @Severity(SeverityLevel.NORMAL)
    @Test(groups = "feed")
    public void T15_VerifyPicturesAreDisplayedInMyPhotosSection() {
        actions.logIn(Constants.USER_A_USERNAME, Constants.ALL_USERS_VALID_PASSWORD);
        actions.goToUsersPage();
        actions.uploadPictureInPost();
        actions.createPost("post.postPublic", Constants.POST_EXAMPLE);
        actions.clickElement("post.myPhotosBtn");
        actions.assertElementPresent("post.uploadedPicture");
    }
}
