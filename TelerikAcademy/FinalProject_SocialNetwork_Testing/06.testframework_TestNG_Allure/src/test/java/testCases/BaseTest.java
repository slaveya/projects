package testCases;

import com.telerikacademy.testframework.UserActions;
import com.telerikacademy.testframework.Utils;
import org.testng.annotations.*;

import java.lang.reflect.Method;

public class BaseTest {
    static UserActions actions;

    @BeforeMethod(groups ="smoke")
    public void nameBefore(Method method) {
        Utils.LOG.info("Performing test:" + method.getName());
        actions.gotoHomePage();
    }

    @BeforeClass(groups ="smoke",alwaysRun = true)
    public static void setUp() {
        actions = new UserActions();
        UserActions.loadBrowser();
    }

    @AfterClass(groups ="smoke",alwaysRun = true)
    public static void tearDown() {
        UserActions.quitDriver();
    }

}
