package com.telerikacademy.testframework;

public enum BrowserType {
    CHROME,
    FIREFOX
}