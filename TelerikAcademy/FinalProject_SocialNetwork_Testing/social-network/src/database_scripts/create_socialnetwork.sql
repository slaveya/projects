create schema if not exists socialnetwork collate utf8_general_ci;

use socialnetwork;
create table users
(
    user_id                 int auto_increment
        primary key,
    username                varchar(30)      null,
    first_name              varchar(30)      null,
    last_name               varchar(30)      null,
    password                varchar(68)      null,
    password_confirmation   varchar(68)      null,
    email                   varchar(35)      null,
    profile_photo           longblob         null,
    profile_picture_privacy varchar(25)      null,
    gender                  varchar(25)      null,
    date_of_birth           date             null,
    country_of_birth        varchar(30)      null,
    town_of_birth           varchar(30)      null,
    country_of_residence    varchar(30)      null,
    town_of_residence       varchar(30)      null,
    activated               bit default b'1' null,
    enabled                 tinyint          null,
    constraint user_Email_uindex
        unique (email),
    constraint user_UserName_uindex
        unique (username)
);

create table authorities
(
    username  varchar(50) not null,
    authority varchar(50) not null,
    constraint username_authority
        unique (username, authority),
    constraint FK__users
        foreign key (username) references users (username)
);

create table buddies
(
    sender   int         not null,
    receiver int         not null,
    status   varchar(30) null,
    primary key (sender, receiver),
    constraint FK_user_buddies
        foreign key (sender) references users (user_id),
    constraint FK_user_buddies_2
        foreign key (receiver) references users (user_id)
);

create table post
(
    post_id       int auto_increment
        primary key,
    text          varchar(250)                       null,
    photo         longblob                           null,
    user_id       int                                null,
    created_at    datetime default CURRENT_TIMESTAMP not null,
    privacy_level varchar(30)                        null,
    constraint FK_user_post
        foreign key (user_id) references users (user_id)
);

create table comment
(
    comment_id   int auto_increment
        primary key,
    text         varchar(250)                       null,
    photo        longblob                           null,
    user_id      int                                null,
    post_id      int                                null,
    comment_time datetime default CURRENT_TIMESTAMP null,
    constraint FK_post_comment
        foreign key (post_id) references post (post_id),
    constraint FK_user_comment
        foreign key (user_id) references users (user_id)
);

create table like_comment
(
    user_id    int not null,
    comment_id int not null,
    primary key (user_id, comment_id),
    constraint FK_comment_like_comment
        foreign key (comment_id) references comment (comment_id),
    constraint FK_user_like_comment
        foreign key (user_id) references users (user_id)
);

create table like_post
(
    user_id int not null,
    post_id int not null,
    primary key (user_id, post_id),
    constraint FK_Post_Likepost
        foreign key (post_id) references post (post_id),
    constraint FK_User_Likepost
        foreign key (user_id) references users (user_id)
);

create table post_comment
(
    post_id    int not null,
    comment_id int not null,
    primary key (post_id, comment_id),
    constraint FK_comment_post_comment
        foreign key (comment_id) references comment (comment_id),
    constraint FK_post_post_comment
        foreign key (post_id) references post (post_id)
);

create table requests
(
    sender_id   int not null,
    receiver_id int not null,
    primary key (sender_id, receiver_id),
    constraint FK_Receiver_RequestsReceived
        foreign key (receiver_id) references users (user_id),
    constraint FK_Sender_RequestsReceived
        foreign key (sender_id) references users (user_id)
);

create table user_friend
(
    user_id   int not null,
    friend_id int not null,
    primary key (user_id, friend_id),
    constraint FK_Friend_Userfriend
        foreign key (friend_id) references users (user_id),
    constraint FK_User_Userfriend
        foreign key (user_id) references users (user_id)
);

create table requests
(
    sender_id   int not null,
    receiver_id int not null,
    primary key (sender_id, receiver_id),
    constraint FK_Receiver_RequestsReceived
        foreign key (receiver_id) references users (user_id),
    constraint FK_Sender_RequestsReceived
        foreign key (sender_id) references users (user_id)
);

