package socialnetwork.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import socialnetwork.models.Post;

import java.util.List;

@Repository
public interface PostRepository extends CrudRepository<Post, Integer> {

    List<Post> findAll();

    @Query("select p from Post p order by created_at desc ")
    List<Post> findAllPosts();

    @Query("select p from Post p where p.privacy like 'PUBLIC' order by postTime desc ")
    List<Post> getPublicFeed();

    List<Post> findAllByUserUsernameOrderByPostTimeDesc(String username);

    @Query("select p from Post p where p.user = :userId")
    List<Post> findUserPostsById(int userId);

}
