package socialnetwork.exceptions;

public class RequestAlreadyReceivedException extends Exception {
    public RequestAlreadyReceivedException() {super("You have already received a buddy request from this user!");}
}
