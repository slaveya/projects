package socialnetwork.exceptions;

public class PostMismatchException extends Exception{
    public PostMismatchException() {
        super("This post doesn't belong to this user!");
    }
}
