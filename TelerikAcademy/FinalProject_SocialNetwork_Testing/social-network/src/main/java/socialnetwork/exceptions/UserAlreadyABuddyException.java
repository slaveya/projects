package socialnetwork.exceptions;

public class UserAlreadyABuddyException extends Exception {
    public UserAlreadyABuddyException() {
        super("You are already buddies with this user!");
    }
}
