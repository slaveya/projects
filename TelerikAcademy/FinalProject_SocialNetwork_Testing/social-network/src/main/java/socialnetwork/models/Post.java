package socialnetwork.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.web.multipart.MultipartFile;
import socialnetwork.models.enums.Privacy;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "post")
public class Post {
    public static final int MINUTES_IN_A_DAY = 1440;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "post_id")
    private int postID;

    @Size(min = 1, max = 500)
    @Column(name = "text")
    private String text;

    @JsonIgnore
    @Lob
    @Column(name = "photo")
    private byte[] picture;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at", nullable = false)
    private Date postTime = new Date();

    @Enumerated(EnumType.STRING)
    @Column(name = "privacy_level")
    private Privacy privacy;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(
            mappedBy = "post",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Comment> comments;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "like_post",
            joinColumns = @JoinColumn(name = "post_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<User> likes;

    public Post(){}

    public Set<User> getLikes() {
        return likes;
    }

    public void setLikes(Set<User> likes) {
        this.likes = likes;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public int getPostID() {
        return postID;
    }

    public void setPostID(int postID) {
        this.postID = postID;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public byte[] getPicture() {
        return picture;
    }

    public boolean hasPicture(){
        if(picture.length < 256){
            return false;
        }
        return true;
    }

    public void setPicture(MultipartFile picture) throws IOException {
        this.picture = picture.getBytes();
    }

    public String printPostPicture(){
        return new String(Base64.encodeBase64(this.getPicture()));
    }

    public Date getPostTime() {
        return postTime;
    }

    public void setPostTime(Date postTime) {
        this.postTime = postTime;
    }

    public String getMinutesSincePosted() {
    return timeFormatter(postTime);
}

    public Privacy getPrivacy() {
        return privacy;
    }

    public void setPrivacy(Privacy privacy) {
        this.privacy = privacy;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    static String timeFormatter(Date date) {
        LocalDateTime now = LocalDateTime.now();
        long minutesPassed = LocalDateTime.ofInstant(date.toInstant(),
                ZoneId.systemDefault()).until(now, ChronoUnit.MINUTES);
        int days = (int) (minutesPassed/ MINUTES_IN_A_DAY);
        int hours = (int) ((minutesPassed % MINUTES_IN_A_DAY)/60);
        int minutes = (int) ((minutesPassed % MINUTES_IN_A_DAY)%60);
        if (minutesPassed > MINUTES_IN_A_DAY) {
            if(days > 1){
                return String.format("%d days ago", minutesPassed / MINUTES_IN_A_DAY);
            }else{
                return String.format("%d day ago", minutesPassed / MINUTES_IN_A_DAY);
            }
        }
        if (minutesPassed > 60){
            if(hours > 1){
                return String.format("%d hours ago", minutesPassed / 60);
            }else{
                return String.format("%d hour ago", minutesPassed / 60);
            }
        }
        return String.format("%d min ago", minutesPassed);
    }
}