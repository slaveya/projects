package socialnetwork.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.web.multipart.MultipartFile;
import socialnetwork.models.enums.Gender;
import socialnetwork.models.enums.Privacy;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int userID;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "user_friend",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "friend_id")
    )
    private List<User> friends;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "requests",
            joinColumns = @JoinColumn(name = "receiver_id"),
            inverseJoinColumns = @JoinColumn(name = "sender_id")
    )
    private List<User> requestsReceived;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "requests",
            joinColumns = @JoinColumn(name = "sender_id"),
            inverseJoinColumns = @JoinColumn(name = "receiver_id")
    )
    private List<User> requestsSent;

    @Size(min = 5, max = 30)
    @Column(name = "username")
    private String username;

    @Size(min = 2, max = 30)
    @Column(name = "first_name")
    private String firstName;

    @Size(min = 2, max = 30)
    @Column(name = "last_name")
    private String lastName;

    @Size(min = 5, max = 70)
    @Column(name = "password")
    private String password;

    @Size(min = 12, max = 35)
    @Email(message = "Not a valid email!")
    @Column(name = "email")
    private String email;

    @Lob
    @Column(name = "profile_photo")
    @JsonIgnore
    private byte[] profilePhoto;

    @Enumerated(EnumType.STRING)
    @Column(name = "profile_picture_privacy")
    private Privacy profilePicturePrivacy;

    @Enumerated(EnumType.STRING)
    @Column(name = "gender")
    private Gender gender;

    @Temporal(TemporalType.DATE)
    @Column(name = "date_of_birth")
    private Date dateOfBirth;

    @Size(min = 2, max = 30)
    @Column(name = "town_of_birth")
    private String townOfBirth;

    @Size(min = 2, max = 30)
    @Column(name = "town_of_residence")
    private String townOfResidence;

    @Size(min = 2, max = 30)
    @Column(name = "country_of_residence")
    private String countryOfResidence;

    @Size(min = 2, max = 30)
    @Column(name = "country_of_birth")
    private String countryOfBirth;

    @OneToMany(mappedBy = "user")
    private List<Post> posts;

    @OneToMany(mappedBy = "user")
    private List<Comment> comments;

    @Column(name = "activated")
    private boolean activated;

    @Size(min = 5, max = 25, message = "Password confirmation is required")
    @Column(name = "password_confirmation")
    private String passwordConfirmation;

    public User() {}

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public byte[] getProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(MultipartFile profilePhoto) throws IOException {
        this.profilePhoto = profilePhoto.getBytes();
    }

    public Privacy getProfilePicturePrivacy() {
        return profilePicturePrivacy;
    }

    public void setProfilePicturePrivacy(Privacy profilePicturePrivacy) {
        this.profilePicturePrivacy = profilePicturePrivacy;
    }

    public String printProfilePicture(){
        return new String(Base64.encodeBase64(this.getProfilePhoto()));
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getTownOfBirth() {
        return townOfBirth;
    }

    public void setTownOfBirth(String townOfBirth) {
        this.townOfBirth = townOfBirth;
    }

    public String getTownOfResidence() {
        return townOfResidence;
    }

    public void setTownOfResidence(String townOfResidence) {
        this.townOfResidence = townOfResidence;
    }

    public String getCountryOfResidence() {
        return countryOfResidence;
    }

    public void setCountryOfResidence(String countryOfResidence) {
        this.countryOfResidence = countryOfResidence;
    }

    public String getCountryOfBirth() {
        return countryOfBirth;
    }

    public void setCountryOfBirth(String countryOfBirth) {
        this.countryOfBirth = countryOfBirth;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public List<User> getFriends() {
        return friends;
    }

    public void setFriends(List<User> friends) {
        this.friends = friends;
    }

    public List<User> getRequestsSent() {
        return requestsSent;
    }

    public void setRequestsSent(List<User> requestsSent) {
        this.requestsSent = requestsSent;
    }

    public List<User> getRequestsReceived() {
        return requestsReceived;
    }

    public void setRequestsReceived(List<User> requestsReceived) {
        this.requestsReceived = requestsReceived;
    }
}