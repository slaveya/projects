package socialnetwork.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import socialnetwork.models.Comment;
import socialnetwork.models.Post;
import socialnetwork.models.User;
import socialnetwork.models.enums.Gender;
import socialnetwork.models.enums.Privacy;
import socialnetwork.services.contracts.CommentService;
import socialnetwork.services.contracts.PostService;
import socialnetwork.services.contracts.UserService;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@RequestMapping("/users")
public class UserController {

    private UserService userService;
    private PostService postService;

    @Autowired
    public UserController(UserService userService, PostService postService) {
        this.userService = userService;
        this.postService = postService;
    }

    @GetMapping("/{userName}")
    public String getUser(@PathVariable String userName, Model model, Principal principal) {
        User userToView = userService.findByUserName(userName);
        User loggedUser = userService.findByUserName(principal.getName());
        model.addAttribute("loggedUser", loggedUser);
        model.addAttribute("user", userToView);
        model.addAttribute("newPost", new Post());
        model.addAttribute("newComment", new Comment());
        model.addAttribute("allPosts", postService.getUserPosts(userToView));
        return "user";
    }

    @GetMapping("/{userName}/user-details")
    public String showUserDetails(@PathVariable String userName, Model model, Principal principal) {
        User user = userService.findByUserName(userName);
        User loggedUser = userService.findByUserName(principal.getName());
        model.addAttribute("loggedUser", loggedUser);
        model.addAttribute("user", user);
        return "user-details";
    }

    @GetMapping("/{userName}/update-info")
    public String editProfileInfo(@PathVariable String userName, Principal principal, Model model) {
        User user = userService.findByUserName(principal.getName());
        User currentUser = userService.findByUserName(userName);
        if (!user.equals(currentUser)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        model.addAttribute("loggedUser", user);

        List<String> gender = new ArrayList<>();
        gender.add(Gender.FEMALE.toString());
        gender.add(Gender.MALE.toString());
        gender.add(Gender.OTHER.toString());
        List<String> profilePicturePrivacy = Stream.of(Privacy.values())
                .map(Enum::name)
                .collect(Collectors.toList());
        model.addAttribute("profilePicturePrivacy", profilePicturePrivacy);
        model.addAttribute("firstName", user.getFirstName());
        model.addAttribute("lastName", user.getLastName());
        model.addAttribute("gender", gender);
        model.addAttribute("dateOfBirth", user.getDateOfBirth());
        model.addAttribute("townOfBirth", user.getTownOfBirth());
        model.addAttribute("countryOfBirth", user.getCountryOfBirth());
        model.addAttribute("townOfResidence", user.getTownOfResidence());
        model.addAttribute("countryOfResidence", user.getCountryOfResidence());
        model.addAttribute("email", user.getEmail());
        model.addAttribute("profilePhoto", user.getProfilePhoto());
        return "update-info";
    }

    @PutMapping("/{userName}/update-info")
    public String updateUser(@PathVariable String userName, @ModelAttribute @Valid User loggedUser,
                             BindingResult bindingResult, Principal principal, Model model) {
        User user = userService.findByUserName(principal.getName());
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "All fields must be correctly filled!");
            return "update-info";
        }
        loggedUser.setUserID(user.getUserID());
        loggedUser.setUsername(user.getUsername());
        loggedUser.setPassword(user.getPassword());
        loggedUser.setActivated(true);
        try {
            userService.updateUser(loggedUser);
        }
        catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        return "redirect:/users/" + userName + "/user-details";
    }

    @DeleteMapping("/{userName}")
    public String deleteUser(@PathVariable String userName) {
        userService.deleteUser(userName);
        return "redirect:/users";
    }

    @GetMapping("/friend/{userName}")
    public String addBuddy(@PathVariable String userName, Principal principal) {
        userService.addBuddy(principal.getName(),userName);
        return "redirect:/users/" + userName;
    }

    @GetMapping("/unFriend/{userName}")
    public String removeBuddy(@PathVariable String userName, Principal principal) {
        userService.removeBuddy(principal.getName(),userName);
        return "redirect:/users/" + userName;
    }

    @GetMapping("/sentRequest/{userName}")
    public String sentRequest(@PathVariable String userName, Principal principal) {
        userService.sendRequest(userName, principal.getName());
        return "redirect:/users/" + userName;
    }

    @GetMapping("/cancelRequest/{userName}")
    public String cancelRequest(@PathVariable String userName, Principal principal) {
        userService.cancelRequest(userName, principal.getName());
        return "redirect:/users/" + userName;
    }

    @GetMapping("/denyRequest/{userName}")
    public String denyRequest(@PathVariable String userName, Principal principal) {
        userService.denyRequest(userName, principal.getName());
        return "redirect:/users/" + userName;
    }
}