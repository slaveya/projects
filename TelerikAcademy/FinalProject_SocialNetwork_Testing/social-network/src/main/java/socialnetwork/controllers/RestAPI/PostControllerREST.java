package socialnetwork.controllers.RestAPI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import socialnetwork.models.Comment;
import socialnetwork.models.Post;
import socialnetwork.models.User;
import socialnetwork.services.contracts.CommentService;
import socialnetwork.services.contracts.PostService;
import socialnetwork.services.contracts.UserService;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/api/posts")
public class PostControllerREST {

    private UserService userService;
    private PostService postService;
    private CommentService commentService;

    @Autowired
    public PostControllerREST(UserService userService, PostService postService, CommentService commentService) {
        this.userService = userService;
        this.postService = postService;
        this.commentService = commentService;
    }

    @GetMapping
    public List<Post> getAll() {
        return postService.findAll();
    }

    @GetMapping("/{id}")
    public Post getById(@PathVariable int id) {
        Post post = postService.findPostByID(id);
        return post;
    }

    @GetMapping("/{id}/comments")
    public List<Comment> getComments(@PathVariable int id) {
        Post post = postService.findPostByID(id);
        return post.getComments();
        //return postService.getPostComments(post);
    }

    @GetMapping("byUser/{userName}")
    public List<Post> getUserPosts(@PathVariable String userName) {
        User user = userService.findByUserName(userName);
        return postService.getUserPosts(user);
    }

    @PostMapping("/{userName}")
    public Post create(@PathVariable String userName, @RequestBody @Valid Post post){
        try {
            postService.createPost(post, userName);
            return post;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

//    @PutMapping("/{id}")
//    public Post update(
//            @PathVariable int id,
//            @RequestBody @Valid Post post) throws PostMismatchException {
//        postService.updatePost(postService.findPostByID(id), post.getText(), post.getPicture());
//        return post;
//    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            postService.removePost(id);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/{postID}/comment/{userName}")
    public Comment createComment(@PathVariable int postID,@PathVariable String userName,
                                @RequestBody @Valid Comment comment) {
        try {
            commentService.createComment(userName, postID, comment);
            return comment;
        }
        catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{postID}/like/{userName}")
    public Post likePost(@PathVariable int postID, @PathVariable String userName) {
        Post post = postService.findPostByID(postID);
        postService.likePost(userName, post);
        return post;
    }

    @GetMapping("/{postID}/likes")
    public int postLikes(@PathVariable int postID ) {
        return postService.getNumberOfLikes(postID);
    }

    @GetMapping("/publicFeed")
    public List<Post> getPublicFeed(){
        return postService.getPublicFeed();
    }

}