package socialnetwork.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import socialnetwork.models.Comment;
import socialnetwork.models.User;
import socialnetwork.services.contracts.CommentService;
import socialnetwork.services.contracts.PostService;
import socialnetwork.services.contracts.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.security.Principal;

@Controller
public class HomeController {

    private UserService userService;
    private PostService postService;
    private CommentService commentService;

    @Autowired
    public HomeController(UserService userService, PostService postService, CommentService commentService) {
        this.userService = userService;
        this.postService = postService;
        this.commentService = commentService;
    }

    @GetMapping("/")
    public String getHomePage(Model model, Principal principal, Authentication authentication) {
        if ((authentication != null)) {
            User loggedUser = userService.findByUserName(principal.getName());
            model.addAttribute("loggedUser", loggedUser);
            model.addAttribute("newComment", new Comment());
            model.addAttribute("allPosts", postService.getUserFeed(loggedUser));
        }
        return "index";
    }

    @PostMapping("comment/{postID}")
    public String commentOnPost(@Valid @ModelAttribute("newComment") Comment comment,
                                BindingResult bindingResult, @PathVariable int postID, Principal principal, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "You didn't enter any text for the comment");
            return "redirect:/";
        }
        commentService.createComment(principal.getName(), postID, comment);
        return "redirect:/";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout";
    }

    @PostMapping("/users")
    public String searchUsersByNames(@RequestParam String search, Model model, Principal principal) {
        User loggedUser = userService.findByUserName(principal.getName());
        try {
            model.addAttribute("loggedUser", loggedUser);
            model.addAttribute("search", userService.userSearch(search));
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        return "users";
    }
}