package socialnetwork.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import socialnetwork.models.Comment;
import socialnetwork.models.Post;
import socialnetwork.models.User;
import socialnetwork.repositories.CommentRepository;
import socialnetwork.repositories.PostRepository;
import socialnetwork.repositories.UserRepository;
import socialnetwork.services.contracts.CommentService;

import javax.persistence.EntityNotFoundException;

@Service
public class CommentServiceImpl implements CommentService {

    private UserRepository userRepository;
    private CommentRepository commentRepository;
    private PostRepository postRepository;

    @Autowired
    public CommentServiceImpl(UserRepository userRepository, CommentRepository commentRepository, PostRepository postRepository) {
        this.userRepository = userRepository;
        this.commentRepository = commentRepository;
        this.postRepository = postRepository;
    }

    @Override
    public Comment findCommentById(int commentID) {
        return commentRepository.findById(commentID).orElseThrow(() -> new EntityNotFoundException(
                String.format("Comment with id %d does not exist.", commentID)));
    }

    @Override
    public Comment createComment(String userName, int postID, Comment comment) {
        Post post = postRepository.findById(postID)
                .orElseThrow(() -> new EntityNotFoundException("There is no such post!"));
        User user = userRepository.findByUserName(userName);
        if (user == null) {
            throw new EntityNotFoundException("There is no such user!");
        }        
        comment.setPost(post);
        comment.setUser(user);
        return commentRepository.save(comment);
    }

    @Override
    @Transactional
    public Comment updateComment(Comment comment, String text, byte[] photo) {
        Comment commentToUpdate = findCommentById(comment.getCommentID());
        commentToUpdate.setText(text);
        commentToUpdate.setPhoto(photo);

        return comment;
    }

    @Override
    public void deleteComment(Comment comment) {
        commentRepository.delete(comment);
    }

}
