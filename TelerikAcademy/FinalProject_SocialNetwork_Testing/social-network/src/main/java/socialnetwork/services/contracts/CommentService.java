package socialnetwork.services.contracts;

import socialnetwork.models.Comment;

public interface CommentService {

    Comment findCommentById(int commentID);

    Comment createComment(String userName, int postID, Comment comment);

    Comment updateComment(Comment comment, String text, byte[] photo);

    void deleteComment(Comment comment);

}
