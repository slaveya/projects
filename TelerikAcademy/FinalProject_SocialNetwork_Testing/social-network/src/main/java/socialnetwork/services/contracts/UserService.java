package socialnetwork.services.contracts;

import org.springframework.web.multipart.MultipartFile;
import socialnetwork.exceptions.DuplicateUsernameException;
import socialnetwork.models.User;

import java.io.IOException;
import java.util.List;
import java.util.Set;

public interface UserService {

    User findByUserName(String userName);

    List<User> findAll();

    List<User> findAllActivated();

    User createUser(User user) throws DuplicateUsernameException;

    void deleteUser(String userName);

    User updateUser(User user);

    public User changeProfilePic(User user, MultipartFile photo) throws IOException;

    Set<User> userSearch(String search);

    void addBuddy(String sender, String receiver);

    void removeBuddy(String sender, String receiver);

    void sendRequest (String sender, String receiver);

    void denyRequest (String sender, String receiver);

    void cancelRequest(String userName, String name);
}