package socialnetwork.servicetests;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import socialnetwork.exceptions.DuplicateUsernameException;
import socialnetwork.exceptions.RequestAlreadyReceivedException;
import socialnetwork.exceptions.RequestAlreadySentException;
import socialnetwork.exceptions.UserAlreadyABuddyException;
import socialnetwork.models.User;
import socialnetwork.repositories.UserRepository;
import socialnetwork.services.UserServiceImpl;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.Silent.class)
public class UserServiceImplTests {

    @Mock
    UserRepository userMockRepository;
//    @Mock
//    BuddiesRepository buddiesMockRepository;

    @InjectMocks
    UserServiceImpl userService;

    @Test
    public void userList_Should_ReturnCorrectSizeOfUsers() {
        Mockito.when(userMockRepository.findAll())
                .thenReturn(Arrays.asList(
                        new User(),
                        new User(),
                        new User()
                ));

        List<User> result = userService.findAll();

        Assert.assertEquals(3, result.size());
    }

    @Test
    public void create_Should_CreateInRepository_When_UserIsValid() throws DuplicateUsernameException {
        //Arrange
        User user1 = new User();

        //Act
        userService.createUser(user1);

        //Assert
        verify(userMockRepository, times(1)).save(user1);
    }

    @Test
    public void deleteUser_Should_SetUserToInactive_When_UserIsValid() {
        //Arrange
        User user = new User();
        user.setUsername("Pinko");
        when(userMockRepository.findByUserName(anyString())).thenReturn(user);

        //Act
        userService.deleteUser("Pinko");

        //Assert
        Assert.assertFalse(user.isActivated());
    }

    @Test
    public void findByUserName_Should_ReturnTheCorrectUser_When_UserIsValid() {
        //Arrange
        User user = new User();
        user.setUsername("Gosho");
        when(userMockRepository.findByUserName("Gosho")).thenReturn(user);

        //Act
        userService.findByUserName("Gosho");

        //Assert
        verify(userMockRepository, times(1)).findByUserName("Gosho");
    }

    @Ignore
    @Test
    public void buddyRequest_should_beReceived_when_sent() throws RequestAlreadySentException, RequestAlreadyReceivedException, UserAlreadyABuddyException {
        User sender = new User();
        User receiver = new User();

        sender.setUsername("Acho");
        receiver.setUsername("Stenli");
        sender.setActivated(true);
        receiver.setActivated(true);

        when(userService.findByUserName("Acho")).thenReturn(sender);
        when(userService.findByUserName("Stenli")).thenReturn(receiver);

        when(userMockRepository.findByUserName("Acho")).thenReturn(sender);
        when(userMockRepository.findByUserName("Stenli")).thenReturn(receiver);

//        when(userService.sendRequest(sender, receiver)).thenReturn(buddiesMockRepository.save(new Buddies(sender, receiver)));
//
//        userService.sendRequest(sender, receiver);
//
//        when(buddiesMockRepository.findBuddies(receiver)).thenReturn(receiver.getRequestsReceived());

//        Assert.assertTrue(buddiesMockRepository.findBuddies(receiver).contains(buddiesMockRepository.findConnection(sender, receiver)));
    }

    @Test
    public void email_should_beSetProperly() {
        User user = new User();

        user.setEmail("stanislav.iliev2394@gmail.com");

        Assert.assertEquals("stanislav.iliev2394@gmail.com", user.getEmail());
    }

    @Test
    public void findByEmail_should_returnNull_when_invalid() {
        User user = new User();

        user.setEmail("stanislav.iliev2394@gmail.com");

    }
}
