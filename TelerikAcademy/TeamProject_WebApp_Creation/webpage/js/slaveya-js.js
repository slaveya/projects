$(document).ready(function() {
  const potterPrefix = "https://www.potterapi.com/v1/";
  const options = {
    key: "$2a$10$WYnU1IQLIX4XaeSZpT9.Ee1tNkG4f3ljZkqZ08Dqc4tQRMeJ1yKI."
  };
  const sortMeButton = $("#sortMe");
  const sortingResult = $("#sortingResult");
  const showHouseButton = $("#showHouse");
  const houseResult = $("#houseResult");
  
  const audio = new Audio();
  const audioPathPrefix = `./sounds/Slaveya/`;

  let houseName = "";

  sortMeButton.on("click", event => {
    $.get(`${potterPrefix}sortingHat`, options, result => {
      houseName = result;
      sortingResult.html(houseName);
      showHouseButton.removeClass(["hide"]);
      houseResult.removeClass(["hide"]);
    });
  });
  showHouseButton.on("click", event => {
    $.get(`${potterPrefix}houses`, options, result => {
      const currentHouse = result.filter(house => house.name === houseName)[0];
      houseResult.html(JSON.stringify(currentHouse));
    });
  });

  $(".individual-number").on("click", event => {
    audio.src = `${audioPathPrefix}${event.target.dataset.mp3Name}`;
    audio.play();
  });
});
